/* jshint esversion: 8 */
const Sequelize = require('sequelize');

module.exports = {
	up: (query) => {
		return query.addColumn('contract', 'termination_min_date', {type:  Sequelize.DATEONLY, allowNull: true, defaultValue: false});
	},
	down: async (query) => {
		return query.dropColumn('contract', 'termination_min_date');
	}
}