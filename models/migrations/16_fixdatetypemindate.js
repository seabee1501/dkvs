/* jshint esversion: 8 */
const Sequelize = require('sequelize');

module.exports = {
	up: (query) => {
		return query.changeColumn('contract', 'termination_min_date', { type: Sequelize.DATEONLY,	allowNull: true	})
	},
	down: async (query) => {
		throw "Cannot downgrade";
	}
}